package com.website.DataAccess;

import com.website.Entity.Website;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Repository
@Qualifier("fakeData")
public class FakeWebsiteDao implements WebsiteDao {
    private static Map<Integer, Website> websites;

    public static Date dateConverter(String s){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(0000, 0, 00);
        try {
            date = sdf.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    static {
        websites = new HashMap<Integer, Website>(){
            {
                put(1, new Website("amazon", dateConverter("2017-03-01"), 1231));
                put(2, new Website("google", dateConverter("2017-03-02"), 5567));
                put(3, new Website("facebook", dateConverter("2017-03-20"), 1231));
            }
        };
    }

    @Override
    public Collection<Website> getAllWebsites(){
        return this.websites.values();
    }

    @Override
    public Collection<Website> getWebsitesByDate(Date d) {
        return null;
    }

//    public Collection<Websites> getWebsiteByDate(){
//        return this.website.values()
//    }

}
