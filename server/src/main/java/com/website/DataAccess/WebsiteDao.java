package com.website.DataAccess;

import com.website.Entity.Website;

import java.util.Collection;
import java.util.Date;

public interface WebsiteDao {
    Collection<Website> getAllWebsites();
    Collection<Website> getWebsitesByDate(Date d);
}
