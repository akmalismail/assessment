package com.website.DataAccess;

import com.website.Entity.Website;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Repository
@Qualifier("postgres")
public class PostgresWebsiteDao implements WebsiteDao{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static class WebsiteRowMapper implements RowMapper<Website>{
        @Override
        public Website mapRow(ResultSet resultSet, int i) throws SQLException {
            Website website = new Website();
            website.setWebsite(resultSet.getString("website_name"));
            website.setDate(resultSet.getDate("visit_date"));
            website.setVisit(resultSet.getInt("total_visits"));
            return website;
        }
    }

    @Override
    public Collection<Website> getAllWebsites() {
        final String sql = "SELECT * FROM websites";
        List<Website> websites = jdbcTemplate.query(sql, new WebsiteRowMapper());
        return websites;
    }

    @Override
    public Collection<Website> getWebsitesByDate(Date d) {
        final String sql = "SELECT * FROM websites WHERE visit_date = ?";
        List<Website> websites = jdbcTemplate.query(sql, new WebsiteRowMapper(), d);
        return websites;
    }
}
