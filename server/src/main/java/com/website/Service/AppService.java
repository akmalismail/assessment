package com.website.Service;

import com.website.DataAccess.WebsiteDao;
import com.website.Entity.Website;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
public class AppService {

    @Autowired
    @Qualifier("postgres")
    private WebsiteDao websiteDao;

    public Collection<Website> getAllWebsites(){
        return websiteDao.getAllWebsites();
    }
    public Collection<Website> getWebsitesByDate(Date d) {
        return websiteDao.getWebsitesByDate(d);
    }
}
