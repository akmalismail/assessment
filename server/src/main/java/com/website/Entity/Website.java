package com.website.Entity;

import java.util.*;

public class Website {

    private String website;
    private Date date;
    private int visit;

    public Website(){}

    public Website(String w, Date d, int v){
        setWebsite(w);
        setDate(d);
        setVisit(v);
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getVisit() {
        return visit;
    }

    public void setVisit(int visit) {
        this.visit = visit;
    }
}
