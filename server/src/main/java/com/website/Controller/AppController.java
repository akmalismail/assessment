package com.website.Controller;

import com.website.Entity.Website;
import com.website.Service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

@CrossOrigin
@RestController
@RequestMapping("/websites")
public class AppController {

    @Autowired
    private AppService appService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Website> getAllWebsites(){
        return appService.getAllWebsites();
    }

    @RequestMapping(value = "/date", method = RequestMethod.GET)
    public Collection<Website> getWebsitesByDate(@RequestParam("d") @DateTimeFormat(pattern="yyyy-MM-dd") Date d){
        return appService.getWebsitesByDate(d);
    }
}
