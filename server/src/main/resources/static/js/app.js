var app = angular.module('app', []);
var func;

app.controller('getcontroller', function($scope, $http, $location){
    $scope.getFunction = function(date){
        date = date.split('/');
        date = date[2] + "-" + date[1] + "-" + date[0];
//        console.log(date)
        var url = $location.absUrl() + "websites/date?d=" + date;
        $http.get(url).then(function (response){
            $scope.websites = response.data;
//            console.log($scope.websites);
        }, function error(response) {
            $scope.postErrorMessage = "Could not retrieve data.";
            $scope.website = "[]";
//            console.log(response);
        })
    }
    func = $scope.getFunction

})

app.directive('datepicker', function(){
    return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, elem, attrs, ngModelCtrl){
            var updateModel = function(dateText){
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(dateText);
                })
            };
            var options = {
                dateFormat: "dd/mm/yy",
                defaultDate: "01/03/2017",
                constrainInput: true,
                onSelect: function(dateText) {
                    updateModel(dateText);
                    func(dateText);
                }
            };
            elem.datepicker(options);
        }
    }
})