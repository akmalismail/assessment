# Top 5 Website Ranking  
  
This repo contains the assessment for phase 2 of the interview. This app was built using Postgres DB, Spring Framework, and AngularJS.   
  
This app allows users to select a date and displays a list of top 5 website ranking for the particular date. The data is fetched from a postgres database hosted by amazon rds services.  
  
To only install the app, the jar file can be downloaded from this [dropbox link](https://www.dropbox.com/s/qadd0uoqi5jfsal/server-1.0-SNAPSHOT.jar?dl=0) and can be run using:
  
```
# In the folder where the file was downloaded into.
java -jar server-1.0-SNAPSHOT.jar
```
  
To run the server using this repo:
  
```
git clone https://akmalismail@bitbucket.org/akmalismail/assessment.git
java -jar assessment/server/target/server-1.0-SNAPSHOT.jar
```

To view the application, simply go to http://localhost:8080
  
Improvements that can be done:  
- Add more colorful and various animations to the UI to improve the users experience.  
- Add more functions such as adding, deleting and updating the data to the app and the server.  

Here are some images from the app.  
  
Page that comes up when refreshed.  
![Homepage][1]  
  
Selecting a date.   
![SelectDate][2]  
  
Display data onto the table.  
![Display][3]  
  
If there is no data for selected date.  
![NoData][4]  
  
[1]: server/src/main/resources/static/images/1.png "Homepage"

[2]: server/src/main/resources/static/images/2.png "Select a date"

[3]: server/src/main/resources/static/images/3.png "Table"

[4]: server/src/main/resources/static/images/4.png "No data"
